"""
All rights reserved.

Name:      Brandon Staab<bls114@zips.uakron.edu>
Professor: Dr. C-C Chan<chan@uakron.edu>
Class:     Artificial Intelligence and Heuristic Programming 460
Date:      3 November 2019

Project 3: Naïve Bayesian Classifiers
"""

from DataSet import DataSet
from Lexer import Token


class Semantics:
	def __init__(self):
		self.registered_attributes = []

	#### Semantics / Exceptions ################################################

	def lookup_idx_by_label(self, label):
		for idx in range(len(self.registered_attributes)):
			if self.registered_attributes[idx][0] == label:
				return idx
		return None


	def register_attribute_decl(self, attribute_label, states):
		assert type(attribute_label) is str
		assert type(states) is list

		if self.lookup_idx_by_label(attribute_label) is not None:
			labels = ", ".join(self.registered_attributes)
			raise RuntimeError(f" '{attribute_label}' is already registerd in [{labels}].")

		self.registered_attributes += [(attribute_label, states)]


	def require_attribute_state(self, attribute_idx, state):
		assert type(attribute_idx) is int
		assert type(state) is str
		assert attribute_idx in range(len(self.registered_attributes))

		attribute_class = self.registered_attributes[attribute_idx]
		states = attribute_class[1] + ["?"]

		if state not in states:
			raise RuntimeError(f"'{state}' state is not in the attributes class labeded '{attribute_class[0]}'.  Consider its other states: {states}.")

	#### Semantics / Actions ###################################################

	def on_arff(self, relation, attributes, data):
		assert type(relation) is dict
		assert type(attributes) is dict
		assert type(data) is dict

		return DataSet(relation, attributes, data)


	def on_relation(self, relation_identifier):
		assert type(relation_identifier) is str
		return {"relation": relation_identifier}


	def on_label(self, label):
		assert type(label) is str
		return label


	def on_identifier(self, identifier):
		assert type(identifier) is Token
		assert identifier.is_identifier()
		assert identifier.has_lexeme()
		return identifier.get_lexeme()


	def on_string(self, string):
		assert type(string) is Token
		assert string.is_string()
		assert string.has_lexeme()
		return string.get_lexeme()[1:-1]


	def on_attribute_decls(self, attribute_decls):
		assert type(attribute_decls) is list
		return {"attributes": attribute_decls}


	def on_attribute_decl(self, attribute_label, states):
		assert type(attribute_label) is str
		assert type(states) is list
		self.register_attribute_decl(attribute_label, states)
		return [attribute_label, states]


	def on_attribute_label(self, class_label):
		assert type(class_label) is str
		return class_label


	def on_attribute_instance_decl_states(self, attribute_seq):
		assert type(attribute_seq) is list
		return attribute_seq


	def on_attribute_seq(self, attribute_seq):
		assert type(attribute_seq) is list
		return attribute_seq


	def on_attribute(self, label):
		assert type(label) is str
		return label


	def on_data(self, examples):
		assert type(examples) is list
		return {"examples": examples}


	def on_attribute_seq_list(self, attribute_seq_list):
		assert type(attribute_seq_list) is list
		return attribute_seq_list


	def on_example_attribute_seq(self, example):
		assert type(example) is list

		for attribute_idx in range(len(example)):
			self.require_attribute_state(attribute_idx, example[attribute_idx])

		return example

