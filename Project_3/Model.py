"""
All rights reserved.

Name:      Brandon Staab<bls114@zips.uakron.edu>
Professor: Dr. C-C Chan<chan@uakron.edu>
Class:     Artificial Intelligence and Heuristic Programming 460
Date:      3 November 2019

Project 3: Naïve Bayesian Classifiers
"""

from DataSet import DataSet

from copy import deepcopy as copy


#############
# Functions #
#############

## Get #########################################################################

def get_model_data(model):
	assert type(model) is Model
	assert type(model.table) is list
	return model.table[1]


def attribute_data_to_spelling(attribute_data):
	assert type(attribute_data) is list
	return attribute_data[0]


def get_trait_freq_given_label_idx(model, case, attribute_idx, label_idx):
	trait_data = get_trait_data_by_attribute_idx(model, attribute_idx)
	freq = get_trait_frequency(trait_data, label_idx)
	trait_idx = get_trait_idx(case, attribute_idx, trait_data)

	return freq[trait_idx]

## Get / Trait #################################################################

def get_traits(model):
	return get_model_data(model)[:-1]


def get_trait_data_by_attribute_idx(model, attribute_idx):
	return get_traits(model)[attribute_idx]

def trait_data_to_states(trait_data):
	return trait_data[1]


def get_num_trait_states(trait_data):
	x = len(trait_data_to_states(trait_data))


def get_trait_frequency(trait_data, label_idx):
	return trait_data[2 + label_idx]

## Get / Trait / Idx ###########################################################

def get_trait_idx(case, attribute_idx, trait_data):
	assert type(case) is list
	assert type(attribute_idx) is int
	assert type(trait_data) is list

	states = trait_data_to_states(trait_data)
	state = case[attribute_idx]

	if state == "?":
		return None

	for idx in range(len(states)):
		if state == states[idx]:
			return idx
	assert False

## Get / Label #################################################################

def get_label_data(model):
	return get_model_data(model)[-1]


def get_label_state_spellings(model):
	return get_label_data(model)[1]


def get_label_frequency(model):
	return get_label_data(model)[2]


def get_num_label_states(model):
	return len(get_label_state_spellings(model))


def get_label_state_spelling_by_idx(model, idx):
	return get_label_state_spellings(model)[idx]


def label_state_to_idx(model, label_state):
	for idx in range(get_num_label_states(model)):
		if get_label_state_spelling_by_idx(model, idx) == label_state:
			return idx
	assert False


def get_num_labeled(model):
	return sum(get_label_frequency(model))

## Init ########################################################################

def init_model(model, dataset):
	assert type(model) is Model
	assert type(dataset) is DataSet
	assert type(dataset.sym_table) is dict

	d = copy(dataset)
	model.table = [d.get_relation(), make_model_data(d)]


def make_model_data(dataset):
	return make_traits(dataset) + [make_label(dataset)]


def make_traits(dataset):
	n = dataset.get_num_classes_states()
	return [make_trait(x, n) for x in dataset.get_traits()]


def make_trait(trait, num_classes):
	size = len(trait[1])
	tally_space = [[0 for slots in range(size)] for classes in range(num_classes)]
	return trait + tally_space


def make_label(dataset):
	return [dataset.get_classes_label()] + [dataset.get_classes_states()] + [[0] * dataset.get_num_classes_states()]

## Populate ###################################################################

def populate_model(model, dataset):
	assert type(model) is Model
	assert type(dataset) is DataSet
	assert type(model.table) is list
	assert type(dataset.sym_table) is dict
	assert model.get_relation() == dataset.get_relation()

	for example in dataset.get_examples():
		process_example(model, example)


def process_example(model, example):
	assert type(model) is Model
	assert type(example) is list

	labeled_as = example[-1]
	labeled_idx = label_state_to_idx(model, labeled_as)

	for attribute_idx in range(len(example) - 1):
		trait_data = get_trait_data_by_attribute_idx(model, attribute_idx)
		trait_idx = get_trait_idx(example, attribute_idx, trait_data)

		if trait_idx is None:
			continue

		add_trait_occurrence(trait_data, trait_idx, labeled_idx)
	add_label_occurence(model, labeled_idx)

def add_trait_occurrence(trait_data, trait_idx, labeled_idx):
	assert type(trait_data) is list
	assert type(trait_idx) is int
	assert type(labeled_idx) is int
	get_trait_frequency(trait_data, labeled_idx)[trait_idx] += 1


def add_label_occurence(model, labeled_idx):
	get_label_frequency(model)[labeled_idx] += 1

###########
# Classes #
###########

class Model:
	def __init__(self):
		self.table = None


	def predict(self, case):
		fudge = lambda x: 0.000001 if x is 0 else x

		probs = {}
		for label_idx in range(get_num_label_states(self)):
			total_in_label = fudge(get_label_frequency(self)[label_idx])

			p = total_in_label / fudge(get_num_labeled(self))
			for attribute_idx in range(len(case)):
				if case[attribute_idx] == "?":
					continue

				p *= fudge(get_trait_freq_given_label_idx(self, case, attribute_idx, label_idx)) / total_in_label
			probs.update({p: get_label_state_spelling_by_idx(self, label_idx)})
		return probs


	def generate(self, dataset):
		assert type(dataset) is DataSet
		assert type(dataset.sym_table) is dict

		init_model(self, dataset)
		populate_model(self, dataset)

	## Get ####################################################################

	def get_relation(self):
		assert type(self.table) is list
		return self.table[0]


	def get_label_state_spellings(self):
		return get_label_state_spellings(self)


	def get_traits(self):
		return [[attribute_data_to_spelling(x), trait_data_to_states(x)] for x  in get_traits(self)]


	def get_attributes(self):
		return get_model_data(self)


	def get_num_attributes(self):
		return len(self.get_attributes())

