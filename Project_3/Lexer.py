"""
All rights reserved.

Name:      Brandon Staab<bls114@zips.uakron.edu>
Professor: Dr. C-C Chan<chan@uakron.edu>
Class:     Artificial Intelligence and Heuristic Programming 460
Date:      3 November 2019

Project 3: Naïve Bayesian Classifiers
"""

from copy import deepcopy as copy
from enum import Enum, auto


#############
# Functions #
#############

## Character Classes ###########################################################

def is_newline(letter):
	assert type(letter) is str
	assert len(letter) is 1
	return letter is '\n'


def is_whitespace(letter):
	assert type(letter) is str
	assert len(letter) is 1
	return letter in [' ', '\r', '\t']


def is_eof(letter):
	assert type(letter) is str
	assert len(letter) is 1
	return letter == "\0"


def is_commentor(letter):
	assert type(letter) is str
	assert len(letter) is 1
	return letter == "%"


def is_quote(letter):
	assert type(letter) is str
	assert len(letter) is 1
	return letter in ['"', "'"]


def is_alphanumeric(letter):
	assert type(letter) is str
	assert len(letter) is 1
	return str.isalnum(letter)


def is_nondigit(letter):
	assert type(letter) is str
	assert len(letter) is 1
	return letter == "_" or is_alphanumeric(letter)


def is_digit(letter):
	assert type(letter) is str
	assert len(letter) is 1
	return str.isdigit(letter)


def is_missing_data_letter(letter):
	assert type(letter) is str
	assert len(letter) is 1
	return letter is "?"


def is_special(letter):
	assert type(letter) is str
	assert len(letter) is 1
	return letter in ".-+/&[]" or is_missing_data_letter(letter)


def is_valid_for_word(letter):
	assert type(letter) is str
	assert len(letter) is 1
	return is_nondigit(letter) or is_digit(letter) or is_special(letter)

###########
# Classes #
###########

class Location:
	def __init__(self, line, column):
		assert type(line) is int
		assert type(column) is int
		self.line = line
		self.column = column


	def __str__(self):
		return f"line {self.line}, column {self.column}"


	def advance_line(self, amt = 1):
		assert type(amt) is int
		assert amt > 0

		self.line += amt
		return self.line

	def advance_column(self, amt = 1):
		assert type(amt) is int
		assert amt > 0

		self.column += amt
		return self.column


	def next_line(self):
		self.column = 0
		self.advance_line()
		return self


class Token:
	class Kind(Enum):
		eof = auto()
		eol = auto()
		relation = auto()
		attribute = auto()
		data = auto()
		id = auto()
		string_lit = auto()
		at = auto()
		comma = auto()
		lbrace = auto()
		rbrace = auto()


	def __init__(self, location = None, kind = None, lexeme = None):
		if kind is None and lexeme is None:
			assert type(location) is Location or location is None
			self.location = location
			self.kind = Token.Kind.eof
			self.lexeme = None
		else:
			assert type(location) is Location
			assert type(kind) is Token.Kind
			assert type(lexeme) is str or lexeme is None
			self.location = location
			self.kind = kind
			self.lexeme = lexeme


	def __bool__(self):
		return not self.is_eof()


	def __str__(self):
		s = ""
		if self.is_eof():
			s += "`eof"
		else:
			s += f"`{self.kind}"
		if self.lexeme:
			s += f"/{self.lexeme}"
		s += "`"
		if self.location:
			s += " on " + str(self.location)
		return s


	def get_kind(self):
		return self.kind

	#### Token / Location ######################################################

	def get_column(self):
		assert type(self.location) is Location
		return self.location.column


	def get_line(self):
		assert type(self.location) is Location
		return self.location.line


	def advance_column(self, amt = 1):
		assert type(self.location) is Location
		return self.location.advance_column(amt)


	def advance_line(self, amt = 1):
		assert type(self.location) is Location
		return self.location.advance_line(amt)

	#### Token / Kind ##########################################################

	def is_same(self, kinds):
		assert type(kinds) is list
		for kind in kinds:
			assert type(kind) is Token.Kind

		return self.kind in kinds


	def is_not(self, kinds):
		return not self.is_same(kinds)


	def is_eof(self):
		return self.is_same([Token.Kind.eof])


	def is_identifier(self):
		return self.is_same([Token.Kind.id])


	def is_string(self):
		return self.is_same([Token.Kind.string_lit])


	def is_text(self):
		return self.is_same([Token.Kind.string_lit])


	def is_literal(self):
		return self.is_text() or self.is_number()


	def is_keyword(self):
		return self.is_same([Token.Kind.relation, Token.Kind.attribute, Token.Kind.data])


	def is_punctuator(self):
		return self.is_same([Token.Kind.comma, Token.Kind.quote, Token.Kind.lbrace, Token.Kind.rbrace, Token.Kind.at])

	def is_newline(self):
		return self.is_same([Token.Kind.eol])

	#### Token / Lexeme ########################################################

	def has_lexeme(self):
		return self.get_lexeme() is not None


	def get_lexeme(self):
		return self.lexeme


class Lexer:
	def __init__(self, input):
		self.input = input
		self.limit = len(input)

		self.start = None
		self.start_location = None

		self.current = 0
		self.current_location = Location(1, 0)

		self.keywords = {
			"relation": Token.Kind.relation,
			"RELATION": Token.Kind.relation,

			"attribute": Token.Kind.attribute,
			"ATTRIBUTE": Token.Kind.attribute,

			"data": Token.Kind.data,
			"DATA": Token.Kind.data
		}


	def __call__(self):
		return self.scan()


	def scan(self):
		while not self.is_eof():
			letter, self.start, self.start_location = self.next()

			if is_whitespace(letter):
				self.skip_whitespace()
				continue

			elif is_newline(letter):
				return self.lex_newline()

			elif letter is ',':
				return self.lex_monograph(Token.Kind.comma)

			elif is_commentor(letter):
				self.skip_comment();
				continue

			elif is_quote(letter):
				return self.lex_string()

			elif letter is "{":
				return self.lex_monograph(Token.Kind.lbrace)

			elif letter is "}":
				return self.lex_monograph(Token.Kind.rbrace)

			elif letter is '@':
				return self.lex_monograph(Token.Kind.at)

			elif is_valid_for_word(letter):
				return self.lex_word()

			else:
				self.abort(f" Unknown letter '{letter}' encountered.\n -> Consider adding it to the 'is_valid_for_word()' character class in the lexer.")
		return Token(self.start_location)


	def next(self):
		loc = copy(self.current_location)
		loc.advance_column()
		return self.peek(), self.current, loc


	def get_diagnostic(self):
		return f"at {self.current_location}, near '{self.peek_range_rel(7, -3)}'"


	def abort(self, msg = ""):
		raise RuntimeError(f"Fatel lexing error {self.get_diagnostic()}.{msg}")


	def error(self, msg = ""):
		print(f"Lexing error {self.get_diagnostic()}.{msg}")

	#### Lexer / Predicates ####################################################

	def is_eof(self, offset = 0):
		assert type(offset) is int

		location = self.current + offset
		return location not in range(self.limit)


	def peek_is(self, letter):
		assert type(letter) is str
		assert len(letter) is 1

		return self.peek() == letter


	def peek_is_not(self, letter):
		return not self.peek_is(letter)

	#### Lexer / Views #########################################################

	def peek(self, offset = 0):
		assert type(offset) is int

		if self.is_eof(offset):
			return "\0"

		location = self.current + offset
		return self.input[location]


	def peek_range(self, start, end):
		return self.input[start:end]


	def peek_range_rel(self, length, offset = 0):
		assert type(length) is int
		assert type(offset) is int
		assert length > 0

		start = self.current + offset
		end = start + length
		return self.peek_range(start, end)

	#### Lexer / Consumption ###################################################

	def advance(self):
		if self.is_eof():
			self.error(" At the end of the file.")
		self.current += 1
		self.current_location.advance_column()


	def accept(self, amt = 1):
		assert type(amt) is int
		assert not self.is_eof(amt - 1)
		assert amt > 0

		while amt:
			self.advance()
			amt -= 1
		return self.peek()


	def require(self, func):
		assert callable(func)
		assert func(self.peek())
		return self.accept()


	def ignore(self, amt = 1):
		assert type(amt) is int
		assert amt > 0
		return self.accept(amt)


	def skip_whitespace(self):
		assert is_whitespace(self.peek())
		self.ignore()

		while not self.is_eof() and is_whitespace(self.peek()):
			self.ignore()


	def skip_comment(self):
		def next_is_continuation(letter):
			return not is_eof(letter) and is_commentor(letter)

		def is_continuation():
			return not is_newline(self.peek()) or next_is_continuation(self.peek(1))

		assert is_commentor(self.peek())
		self.ignore()

		while not self.is_eof() and is_continuation():
			self.ignore()

	#### Lexer / Matching ######################################################

	def lex_monograph(self, kind):
		self.accept()
		return Token(self.start_location, kind)


	def lex_newline(self):
		assert is_newline(self.peek())
		tok = self.lex_monograph(Token.Kind.eol)
		self.start_location = self.current_location.next_line()

		while not self.is_eof() and is_newline(self.peek()):
			tok = self.lex_monograph(Token.Kind.eol)
			self.start_location = self.current_location.next_line()
		return tok


	def lex_word(self):
		while not self.is_eof() and is_valid_for_word(self.peek()):
			self.accept()

		lexeme = self.peek_range(self.start, self.current)
		kind = self.keywords.get(lexeme, Token.Kind.id)
		return Token(self.start_location, kind, lexeme)


	def lex_string(self):
		quote = self.peek()
		self.require(is_quote)

		self.accept()
		while not self.is_eof() and self.peek_is_not(quote):
			if self.peek_is("\\"):
				self.abort(" Escape sequences not supported.")
			self.accept()

		if self.is_eof():
			self.abort(" Unterminated string literal.")
		self.accept()

		lexeme = self.peek_range(self.start, self.current)
		return Token(self.start_location, Token.Kind.string_lit, lexeme)

