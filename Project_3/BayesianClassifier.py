"""
All rights reserved.

Name:      Brandon Staab<bls114@zips.uakron.edu>
Professor: Dr. C-C Chan<chan@uakron.edu>
Class:     Artificial Intelligence and Heuristic Programming 460
Date:      3 November 2019

Project 3: Naïve Bayesian Classifiers
"""

from DataSet import DataSet
from Model import Model
from Parser import load_arff_dataset

import json


class BayesianClassifier:
	def __init__(self):
		self.dataset = None
		self.model = Model()


	def load_arff(self, dir, filename):
		assert type(dir) is str
		assert type(filename) is str

		self.dataset = load_arff_dataset(dir + filename)


	def generate(self):
		assert type(self.dataset) is DataSet
		self.model.generate(self.dataset)


	def save_model(self, dir, filename):
		assert type(dir) is str
		assert type(filename) is str

		with open(dir + filename + ".bin", "w") as fp:
			json.dump(self.model.table, fp)


	def load_model(self, dir, filename):
		assert type(dir) is str
		assert type(filename) is str

		with open(dir + filename + ".bin", "r") as fp:
			self.model.table = json.load(fp)


	def has_model(self):
		return type(self.model.table) is list


	def get_examples(self):
		assert type(self.dataset) is DataSet
		return self.dataset.get_examples()


	def classify(self, case):
		assert type(case) is list
		assert self.model.get_relation() == self.dataset.get_relation()

		probs = self.model.predict(case)
		x = max(probs)

		return probs[x], x


	def require_compatble_model_and_dataset(self):
		model_relation = self.model.get_relation()
		dataset_relation = self.dataset.get_relation()

		if model_relation != dataset_relation:
			raise RuntimeError(f"Failed to test the '{model_relation}' model with the '{dataset_relation}' dataset.")

		if self.model.get_num_attributes() != self.dataset.get_num_attributes():
			raise RuntimeError(f"Failed to test the '{model_relation}' relation because the model had {self.model.get_num_attributes()} attributes while the dataset had {self.dataset.get_num_attributes()}.")

		for idx, m_attribute, d_attribute in zip(range(self.model.get_num_attributes()), self.model.get_attributes(), self.dataset.get_attributes()):
			if m_attribute[0] != d_attribute[0]:
				raise RuntimeError(f"Failed to test the '{model_relation}' relation because the model's #{idx + 1} attribute is '{m_attribute[0]}', but the dataset's is '{d_attribute[0]}'.")

			if len(m_attribute[1]) != len(d_attribute[1]):
				raise RuntimeError(f"Failed to test the '{model_relation}' relation because the model's '{m_attribute[0]}' attribute had {len(m_attribute[1])} states, but the dataseti's is {len(d_attribute[1])}.")

			for s_idx, m_state, d_state in zip(range(len(m_attribute[1])), m_attribute[1], d_attribute[1]):
				if m_state != d_state:
					raise RuntimeError(f"Failed to test the '{model_relation}' relation because the model's '{m_attribute[0]}' attribute state '{m_state}' does not match the dataset's attribute state '{d_state}'.")

