"""
All rights reserved.

Name:      Brandon Staab<bls114@zips.uakron.edu>
Professor: Dr. C-C Chan<chan@uakron.edu>
Class:     Artificial Intelligence and Heuristic Programming 460
Date:      3 November 2019

Project 3: Naïve Bayesian Classifiers
"""

from Lexer import Lexer, Token
from Semantics import Semantics


#############
# Functions #
#############

def load_arff_dataset(filename):
	stream = open(filename + ".arff", "r").read()
	return Parser(stream).parse_arff()

###########
# Classes #
###########

class Parser:
	def __init__(self, input):
		lexer = Lexer(input)
		self.act = Semantics()

		self.tokens = []
		while not lexer.is_eof():
			tok = lexer()
			self.tokens += [tok]

		self.lookahead_idx = 0
		self.last = len(self.tokens)


	def peek(self, offset = 0):
		assert type(offset) is int
		idx = self.lookahead_idx + offset

		if idx not in range(self.last):
			return Token()
		return self.tokens[idx]


	def lookahead(self, offset = 0):
		assert type(offset) is int
		return self.peek(offset).get_kind()


	def abort(self, reason = ""):
		assert type(reason) is str
		raise RuntimeError(f"Parsing error at '{self.peek()}'.{reason}")

	#### Parser / Predicates ###################################################

	def is_eof(self, offset = 0):
		return self.lookahead_idx + offset not in range(self.last)


	def next_token_is(self, kind_first, kind_second = None):
		assert type(kind_first) is Token.Kind or kind_first is None
		assert type(kind_second) is Token.Kind or kind_second is None

		if self.lookahead() != kind_first:
			return False
		if kind_second is not None:
			return self.lookahead(1) == kind_second
		return True


	def next_token_is_not(self, kind, kind_second = None):
		return not self.next_token_is(kind)

	#### Parser / Consumption ##################################################

	def consume(self):
		assert self.lookahead_idx <= self.last

		tok = self.peek()
		self.lookahead_idx += 1
		return tok

	#### Parser / Matching #####################################################

	def match(self, kind):
		assert type(kind) is Token.Kind

		if self.lookahead() is kind:
			return self.consume()
		self.abort(f" '{kind}' was expected.")


	def match_if(self, kind):
		if self.lookahead() is kind:
			return self.match(kind)
		return Token()


	def require(self, kind):
		assert self.lookahead() is kind
		return self.match(kind)

	#### Parser / Parsing ######################################################

	def parse_eols(self):
		# <EOLs> ::= <EOL>
		#          | <EOL> <EOLs>
		self.match(Token.Kind.eol)
		self.parse_opt_eols()


	def parse_opt_eols(self):
		# <opt-EOLs> ::= ""
		#              | <EOL> <opt-EOLs>
		while self.match_if(Token.Kind.eol):
			pass


	def parse_arff(self):
		# <arff> ::= <relation> <attribute-decls> <data> <EOF>
		relation = self.parse_relation()
		attribute_decls = self.parse_attribute_decls()
		data = self.parse_data()

		if self.lookahead() is not Token.Kind.eof:
			self.abort(" Unexpected data after the data segment.")
		self.require(Token.Kind.eof)

		return self.act.on_arff(relation, attribute_decls, data)


	def parse_relation(self):
		# <relation> ::= <opt-EOLs> '@' 'relation' <label> <opt-EOLs>
		self.parse_opt_eols()
		self.match(Token.Kind.at)
		self.match(Token.Kind.relation)
		relation_identifier = self.parse_label()
		self.parse_opt_eols()

		return self.act.on_relation(relation_identifier)


	def parse_label(self):
		# <label> ::= <identifier>
		#           | <string>
		if self.next_token_is(Token.Kind.string_lit):
			return self.act.on_label(self.parse_string())
		elif self.next_token_is(Token.Kind.id):
			return self.act.on_label(self.parse_identifier())
		else:
			self.abort(f" 'string' or 'identifier' was expected.")


	def parse_identifier(self):
		# <identifier> ::= ...
		identifier = self.match(Token.Kind.id)

		return self.act.on_identifier(identifier)


	def parse_string(self):
		# <string> ::= ...
		string = self.match(Token.Kind.string_lit)

		return self.act.on_string(string)


	def parse_attribute_decls(self):
		# <attribute-decls> ::= <attribute-decl>
		#                     | <attribute-decl> <attribute-decls>
		attribute_decls = []
		while self.next_token_is(Token.Kind.at, Token.Kind.attribute):
			attribute_decls += [self.parse_attribute_decl()]

		return self.act.on_attribute_decls(attribute_decls)


	def parse_attribute_decl(self):
		# <attributes> ::= '@' 'attribute' <attribute-label> '{' <attribute-instance-states-decl> '}' <EOLs>
		self.require(Token.Kind.at)
		self.require(Token.Kind.attribute)
		attribute_label = self.parse_attribute_label()
		self.match(Token.Kind.lbrace)
		states = self.parse_attribute_instance_states_decl()
		self.match(Token.Kind.rbrace)
		self.parse_eols()

		return self.act.on_attribute_decl(attribute_label, states)


	def parse_attribute_label(self):
		# <attribute-label> ::= <label>
		class_identifier = self.parse_label()

		try:
			return self.act.on_attribute_label(class_identifier)
		except RuntimeError as ex:
			self.abort(str(ex))


	def parse_attribute_instance_states_decl(self):
		# <attribute-instance-states-decl> ::= <attribute-seq>
		attribute_seq = self.parse_attribute_seq()

		return self.act.on_attribute_instance_decl_states(attribute_seq)


	def parse_attribute_seq(self):
		# <attribute-seq> ::= <attribute>
		#                   | <attribute> ',' <attribute-seq>
		attribute_seq = [self.parse_attribute()]

		while self.match_if(Token.Kind.comma):
			attribute_seq += [self.parse_attribute()]

		return self.act.on_attribute_seq(attribute_seq)


	def parse_attribute(self):
		# <attribute> ::= <label>
		label = self.parse_label()

		return self.act.on_attribute(label)


	def parse_data(self):
		# <data> ::= '@' 'data' <EOLs> <attribute-seq-list> <opt-EOLs
		self.match(Token.Kind.at)
		self.match(Token.Kind.data)
		self.parse_eols()
		examples = self.parse_attribute_seq_list()
		self.parse_opt_eols()

		return self.act.on_data(examples)


	def parse_attribute_seq_list(self):
		# <attribute-seq-list> ::= <example-attribute-seq>
		#                        | <example-attribute-seq> <EOL> <attribute-seq-list>
		attribute_seq_list = [self.parse_example_attribute_seq()]
		while self.match_if(Token.Kind.eol) and not self.is_eof(1):
			attribute_seq_list += [self.parse_example_attribute_seq()]

		return self.act.on_attribute_seq_list(attribute_seq_list)


	def parse_example_attribute_seq(self):
		# <example-attribute-seq> ::= <attribute-seq>
		attribute_seq = self.parse_attribute_seq()

		return self.act.on_example_attribute_seq(attribute_seq)

