"""
All rights reserved.

Name:      Brandon Staab<bls114@zips.uakron.edu>
Professor: Dr. C-C Chan<chan@uakron.edu>
Class:     Artificial Intelligence and Heuristic Programming 460
Date:      3 November 2019

Project 3: Naïve Bayesian Classifiers
"""


class DataSet:
	def __init__(self, relation, attributes, examples):
		assert type(relation) is dict
		assert type(attributes) is dict
		assert type(examples) is dict

		self.sym_table = {**relation, **attributes, **examples}


	def get_relation(self):
		assert type(self.sym_table) is dict
		return self.sym_table["relation"]


	def get_attributes(self):
		assert type(self.sym_table) is dict
		return self.sym_table["attributes"]


	def get_num_attributes(self):
		return len(self.get_attributes())


	def get_traits(self):
		return self.get_attributes()[:-1]


	def get_classes(self):
		return self.get_attributes()[-1]


	def get_classes_label(self):
		return self.get_classes()[0]


	def get_classes_states(self):
		return self.get_classes()[1]


	def get_num_classes_states(self):
		return len(self.get_classes_states())


	def get_examples(self):
		assert type(self.sym_table) is dict
		return self.sym_table["examples"]

