#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
All rights reserved.

Name:      Brandon Staab<bls114@zips.uakron.edu>
Professor: Dr. C-C Chan<chan@uakron.edu>
Class:     Artificial Intelligence and Heuristic Programming 460
Date:      3 November 2019

Project 3: Naïve Bayesian Classifiers
"""

from BayesianClassifier import BayesianClassifier

import os


##########
# Config #
##########

# Allows the default dataset directory to be changed.
#   Example: datasets_dir = "./"
datasets_dir = "./datasets/"

# Allows the default model directory to be changed.
#  Example: models_dir = "./"
models_dir = "./models/"

########
# Core #
########

## Learn #######################################################################

def learn(classifier):
	filename = get_arff_filename()
	classifier.load_arff(datasets_dir, filename)
	classifier.generate()
	classifier.save_model(models_dir, filename)

## Test ########################################################################

def test(classifier):
	classifier.load_model(models_dir, get_model_filename())
	classifier.load_arff(datasets_dir, get_arff_filename())
	matrix, labels = generate_confusion_matrix(classifier)
	print_confusion_matrix(matrix, labels)


def generate_confusion_matrix(classifier):
	classifier.require_compatble_model_and_dataset()

	labels = classifier.model.get_label_state_spellings()
	size = len(labels)
	matrix = [[0 for x in range(size)] for y in range(size)]

	for example in classifier.get_examples():
		actual = example[-1]
		predicted, percent = classifier.classify(example[:-1])
		# display_classification(example, predicted, percent)
		matrix[labels.index(predicted)][labels.index(actual)] += 1

	return matrix, labels


def print_confusion_matrix(matrix, labels):
	assert type(matrix) is list
	assert type(labels) is list
	assert len(matrix) is len(labels)
	assert len(matrix[0]) is len(labels)

	n = len(matrix)
	correct = 0
	total = 0
	s = "          Actual\nPredicted"
	for state in labels:
		s += f" {state[:6] : >6}"
	s += "\n"

	for x in range(n):
		s += f"{labels[x][:9] : >9}"
		for y in range(n):
			found = matrix[x][y]
			s += f" {found : 6}"

			total += found
			if x == y:
				correct += found
		s += "\n"
	s += f"{correct} correct out of {total} with {correct / total * 100 :.2f}% accuracy."
	print(s)


## Apply #######################################################################

def apply(classifier):
	if not classifier.has_model():
		print("Please load a model and then try again.")
		return

	choice = True
	while choice:
		case = prompt_case(classifier)
		classed_as, percent = classifier.classify(case)
		display_classification(case, classed_as, percent)
		choice = ask_yes_or_no("Would you like to apply the classifier to another test case?")


def prompt_case(classifier):
	assert classifier.has_model()
	return [prompt_state(label, states + ["?"]) for label, states in classifier.model.get_traits()]


def prompt_state(label, states):
	assert type(label) is str
	assert type(states) is list

	print(f"\nSelect {label}:")
	for idx in range(len(states)):
		print(f"{idx + 1 : 3}. {states[idx]}")

	return states[require_bounded_int(1, len(states)) - 1]


def display_classification(case, classed_as, percent):
	print(f"\n{case} -> '{classed_as}'")
	# print(f"\n{case} -> '{classed_as}' at {percent * 100 :.3f}%")

##################
# User Interface #
##################

## Boolean #####################################################################

def ask_yes_or_no(msg):
	assert type(msg) is str
	assert len(msg) > 0

	print(f"""
{msg}
  1. Yes
  2. No""")
	return require_bounded_int(1, 2) is 1

## Integer #####################################################################

def require_int():
	try:
		return int(input())
	except ValueError:
		print("Invalid input. Enter an integer: ", end="")
		return require_int()


def require_bounded_int(min, max):
	assert type(min) is int
	assert type(max) is int
	assert min <= max

	print(f"Enter an integer between {min} and {max}: ", end="")
	choice = require_int()

	if choice not in range(min, max + 1):
		return require_bounded_int(min, max)
	return choice

## Filenames ###################################################################

def get_filename(msg, dir,  ext):
	assert type(msg) is str
	assert type(dir) is str
	assert type(ext) is str
	assert len(msg) > 0
	assert len(dir) > 0
	assert len(ext) >= 0

	print(msg, end="")
	filename, file_extension = os.path.splitext(input())

	if len(file_extension) is not 0 and file_extension != ext:
		print(f"File extension must be '{ext}'. ", end="")
		return get_filename(msg, dir, ext)

	if os.path.isfile(dir + filename + ext):
		print(f"File found: {filename}")
		return filename

	print(f"File '{dir + filename + ext}' not found. ", end="")
	return get_filename(msg, dir, ext)


def get_arff_filename():
	recommend_files(datasets_dir, ".arff")
	return get_filename("Enter a data set's filename: ", datasets_dir, ".arff")


def get_model_filename():
	recommend_files(models_dir, ".bin")
	return get_filename("Enter a model's filename: ", models_dir, ".bin")

## Files & Folders #############################################################

def recommend_files(path, ext):
	file_list = get_file_list(path, ext)
	if len(file_list) is not 0:
		files = "\n  ".join([just_file(f) for f in file_list])
		print("\nRecommended Files:\n  " + files)


def get_file_list(path, ext):
	assert type(ext) is str

	only_files = [os.path.join(path, f) for f in sorted(os.listdir(path)) if os.path.isfile(os.path.join(path, f))]
	if len(ext) > 0:
		only_files = [f for f in only_files if os.path.splitext(f)[1] == ext]
	return only_files


def just_file(path):
	assert type(path) is str
	drive, file_and_path = os.path.splitdrive(path)
	path, file = os.path.split(file_and_path)
	return file


def touch_dirs():
	if not os.path.exists(datasets_dir):
		os.makedirs(datasets_dir)

	if not os.path.exists(models_dir):
		os.makedirs(models_dir)

## Menus #######################################################################

def get_menu_choice():
	print("""
Main Menu:
  1. Learn a naïve Bayesian classifier from data.
  2. Load and test accuracy of a naïve Bayesian classifier.
  3. Apply a naïve Bayesian classifier to new cases.
  4. Quit.""")
	return require_bounded_int(1, 4) - 1


def quit(classifier):
	if ask_yes_or_no("Are you sure you want to quit?"):
		exit()

###################
# Driver Function #
###################

def py_nb():
	touch_dirs()
	classifier = BayesianClassifier()

	while True:
		try:
			[learn, test, apply, quit][get_menu_choice()](classifier)
		except RuntimeError as ex:
			print(f"\aException: {ex}")
			classifier = BayesianClassifier()


if __name__ == "__main__":
	py_nb()

