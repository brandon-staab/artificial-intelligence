#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
All rights reserved.

Name:      Brandon Staab<bls114@zips.uakron.edu>
Professor: Dr. C-C Chan<chan@uakron.edu>
Class:     Artificial Intelligence and Heuristic Programming 460
Date:      6 October 2019

Project 2: Tic-Tac-Toe

A python implementation of the tic-tac-toe game from David Tourezky's Lisp
version, augmented with a rule based artificial intelligence.
"""

import random

#############
#  Globals  #
#############

_computer = 10
_opponent = 1
_triplets = [(1, 2, 3), (4, 5, 6), (7, 8, 9), (1, 4, 7), (2, 5, 8), (3, 6, 9), (1, 5, 9), (3, 5, 7)]
_quadruplets = [(1, 2, 3, 4), (5, 6, 7, 8), (9, 10, 11, 12), (13, 14, 15, 16), (1, 5, 9, 13), (2, 6, 10, 14), (3, 7, 11, 15), (4, 8, 12, 16), (1, 6, 11, 16), (13, 10, 7, 4)]

###########
#  Board  #
###########

def make_board(meta):
	assert type(meta) is list
	assert type(meta[2]) is int

	game_space = [0] * meta[2] ** 2
	return [meta] + game_space


def board_full_p(board):
	assert type(board) is list

	return not 0 in board


def get_meta(board):
	assert type(board) is list

	return board[0]


def get_size(board):
	assert type(board) is list

	return get_meta(board)[2]


def get_total_cells(board):
	assert type(board) is list

	return get_size(board) ** 2


def is_player_ones_turn(board):
	assert type(board) is list

	return get_meta(board)[0] == 0


def get_strategy(board):
	assert type(board) is list

	return get_meta(board)[1]


def player_two_is_user(board):
	assert type(board) is list

	return get_strategy(board) == "user"


def was_users_turn(board):
	assert type(board) is list

	return is_player_ones_turn(board) or player_two_is_user(board)

################
#  Game Logic  #
################

def sum_state(board, state):
	assert type(board) is list
	assert type(state) is tuple

	if get_size(board) == 3:
		assert len(state) == 3
		return board[state[0]] + board[state[1]] + board[state[2]]
	elif get_size(board) == 4:
		assert len(state) == 4
		return board[state[0]] + board[state[1]] + board[state[2]] + board[state[3]]
	else:
		assert False


def get_winning_states(board):
	assert type(board) is list

	return _triplets if get_size(board) == 3 else _quadruplets


def compute_sums(board):
	assert type(board) is list

	return [sum_state(board, state) for state in get_winning_states(board)]


def is_winner(board):
	assert type(board) is list

	n = get_size(board)
	sums = compute_sums(board)
	return n * _computer in sums or n * _opponent in sums


def switch_turn(board):
	assert type(board) is list

	get_meta(board)[0] = 1 if is_player_ones_turn(board) else 0
	return board


def next_turn(board):
	assert type(board) is list

	if is_winner(board):
		if was_users_turn(board):
			print("You win!")
		else:
			print("I win!")

	elif board_full_p(board):
		print("Tie game.")

	else:
		if not is_player_ones_turn(board) or player_two_is_user(board):
			return opponent_move(switch_turn(board))
		else:
			assert is_player_ones_turn(board)
			computer_move(switch_turn(board))


def make_move(player, pos, board):
	assert type(player) is int
	assert player in [_opponent, _computer]
	assert type(pos) is int
	assert pos >= 1
	assert pos <= get_total_cells(board)
	assert type(board) is list

	board[pos] = player
	return board


def opponent_move(board):
	assert type(board) is list

#	print("in opponent:")
#	print(board)
	pos = read_a_legal_move(board)
	assert type(pos) is int
	assert board[pos] == 0

	players_move = _opponent if is_player_ones_turn(board) else _computer
	new_board = make_move(players_move, pos, board)
	print_board(new_board)
	next_turn(new_board)

####################
#  Computer Logic  #
####################

def computer_move(board):
	assert type(board) is list

#	print("In computer move:")
#	print(board)
	best_move = choose_best_move(board)
	pos = best_move[0]
	strategy = best_move[1]

	new_board = make_move(_computer, pos, board)
	print(f"My move: {pos}")
	print(f"My strategy: {strategy}")
	print_board(new_board)
	next_turn(new_board)


def choose_best_move(board):
	assert type(board) is list

#	print("choose move: ")
#	print(board)

	strategy = get_strategy(board)
	if strategy == "random":
		return random_move_strategy(board)
	elif strategy == "rule_based":
		return rule_based_strategy(board)
	else:
		raise RuntimeError(f"invalid strategy: {strategy}")

##########################
#  Random Move Strategy  #
##########################

def random_move_strategy(board):
	assert type(board) is list

	pos = pick_random_empty_position(board)
#	print("value of pos in random move strategy: " + repr(pos))
	return [pos, "random move"]


def pick_random_empty_position(board):
	assert type(board) is list

	n = get_total_cells(board)
	pos = random.choice(list(range(n))) + 1
	if board[pos] == 0:
		return pos
	else:
		return pick_random_empty_position(board)

#########################
#  Rule Based Strategy  #
#########################

def rule_based_strategy(board):
	assert type(board) is list

	pos = pick_rule_based_empty_position(board)
	return [pos, "rule based"]


def pick_rule_based_empty_position(board):
	assert type(board) is list

	if can_win(board):
		# print("CPU can win!")
		return pick_win_move(board)
	elif can_block(board):
		# print("CPU can block!")
		return pick_block_move(board)
	else:
		return pick_good_move(board, get_size(board))


def in_a_row(board, player, num):
	assert type(board) is list
	assert type(player) is int
	assert type(num) is int

	x = num * player
	return 0 != len([sum for sum in compute_sums(board) if sum == x])


def pick_missing(board, player, n):
	assert type(board) is list
	assert type(player) is int
	assert type(n) is int

	goal = player * n
	pack = zip(compute_sums(board), range(len(get_winning_states(board))))

	canidates = [x[1] for x in pack if x[0] == goal]
	if len(canidates) == 0:
		return -1

	idx = canidates[0]
	tup = get_winning_states(board)[idx]
	pos = [pos for pos in tup if board[pos] == 0]
	return pos[0]


def can_win(board):
	assert type(board) is list

	return in_a_row(board, _computer, get_size(board) - 1)


def can_block(board):
	assert type(board) is list

	return in_a_row(board, _opponent, get_size(board) - 1)


def pick_win_move(board):
	assert type(board) is list

	return pick_missing(board, _computer, get_size(board) - 1)


def pick_block_move(board):
	assert type(board) is list

	return pick_missing(board, _opponent, get_size(board) - 1)


def pick_good_move(board, n):
	assert type(board) is list
	assert type(n) is int

	if n <= 1:
		return pick_random_empty_position(board)

	pos = pick_missing(board, _computer, n - 1)
	if pos == -1:
		return pick_good_move(board, n - 1)
	return pos

####################
#  User Interface  #
####################

def read_a_legal_move(board):
	assert type(board) is list

	max_size = get_total_cells(board)
	player = "Your"
	if player_two_is_user(board):
		player = "Player one's" if is_player_ones_turn(board) else "Player two's"
	pos = require_int(f"{player} move: (enter a number between 1 and {max_size}) ")

	if not (pos >= 1 and pos <= max_size):
		print("Invalid input.")
		return read_a_legal_move(board)
	elif board[pos] != 0:
		print("That space is already occupied.")
		return read_a_legal_move(board)
	else:
		return pos


def require_int(prompt=None):
	assert type(prompt) is str or prompt is None

	try:
		return int(input(prompt if prompt else "Enter integer: "))
	except ValueError:
		print("Invalid input, an integer is required.")
		return require_int(prompt)


def convert_to_letter(v):
	assert type(v) is int

	if v == 1:
		return "O"
	elif v == 10:
		return "X"
	else:
		return " "


def print_row(w, x, y, z = None):
	assert type(w) is int
	assert type(x) is int
	assert type(y) is int
	assert type(z) is int or z is None

	if z is None:
		print(f"\t  {convert_to_letter(w)}  |  {convert_to_letter(x)}  |  {convert_to_letter(y)}  ")
	else:
		print(f"\t  {convert_to_letter(w)}  |  {convert_to_letter(x)}  |  {convert_to_letter(y)}  |  {convert_to_letter(z)}  ")


def print_board(board):
	assert type(board) is list

	if get_size(board) == 3:
		print_row(board[1], board[2], board[3])
		print("\t-----------------")
		print_row(board[4], board[5], board[6])
		print("\t-----------------")
		print_row(board[7], board[8], board[9])
	elif get_size(board) == 4:
		print_row(board[1], board[2], board[3], board[4])
		print("\t-----------------------")
		print_row(board[5], board[6], board[7], board[8])
		print("\t-----------------------")
		print_row(board[9], board[10], board[11], board[12])
		print("\t-----------------------")
		print_row(board[13], board[14], board[15], board[16])
	else:
		assert False


def ask_strategy():
	print("""Pick a strategy:
	1. Beginner (Random)
	2. Expert (two-in-a-row + block oppenent)""")

	choice = require_int()
	if choice not in [1, 2]:
		print("Invalid input.")
		return ask_strategy()
	return "rule_based" if choice == 2 else "random"


def ask_size():
	print("""What size board?
	1. 3x3
	2. 4x4""")

	choice = require_int()
	if choice not in [1, 2]:
		print("Invalid input.")
		return ask_size()
	return choice + 2


def play_one_game(choice):
	assert type(choice) is int

	if choice == 1:
		move_first = input("Would you like to go first? (y/n)") == "y"
		if move_first:
			opponent_move(make_board([0, ask_strategy(), ask_size()]))
		else:
			computer_move(make_board([1, ask_strategy(), ask_size()]))
	elif choice == 2:
		opponent_move(make_board([0, "user", ask_size()]))
	else:
		assert False


def play_many_games():
	print("""Main Menu:
	1. User vs. Computer
	2. User vs. User
	3. Quit""")

	choice = require_int()
	if choice == 3:
		return
	elif choice in [1, 2]:
		play_one_game(choice)
	else:
		print("Invalid input.")
	play_many_games()


if __name__ == "__main__":
	play_many_games()
