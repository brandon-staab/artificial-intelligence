"""
All rights reserved.

Name:      Brandon Staab<bls114@zips.uakron.edu>
Professor: Dr. C-C Chan<chan@uakron.edu>
Class:     Artificial Intelligence and Heuristic Programming 460
Date:      22 September 2019

Project 1: Python Functions
"""

# Helper Functions

def identity(x):
	return x


def if_type_else(t, f, g, x):
	assert(type(t) is type)
	assert(callable(f))
	assert(callable(g))

	return f(x) if type(x) is t else g(x)


def if_type(t, f, x):
	assert(type(t) is type)
	assert(callable(f))

	return if_type_else(t, f, identity, x)


def if_list_else(f, g, x):
	assert(callable(f))
	assert(callable(g))

	return if_type_else(list, f, g, x)


def if_list(f, x):
	assert(callable(f))

	return if_list_else(f, identity, x)


def wrap_list(x):
	return [x]


def ensure_list(x):
	return if_list_else(identity, wrap_list, x)



def flatten_lists(x):
	assert(type(x) is list)
	for i in x:
		assert(type(i) is list)

	return [i for s in x for i in s]


def make_seq_of_flattened_lists(x):
	return [ensure_list(if_list(flatten, i)) for i in ensure_list(x)]


def only_types(x, ts):
	for i in ensure_list(ts):
		assert(type(i) is type)

	return [i for i in ensure_list(x) if type(i) in ensure_list(ts)]


def only_ints(x):
	return only_types(x, int)


def none_if_empty(x):
	assert(type(x) is list)

	return None if len(x) == 0 else x

# Test Helper Functions

def test_identity():
	assert(identity(int) is int)
	assert(identity(None) is None)


def test_if_type_else():
	assert(if_type_else(list, min, max, [1, 2]) == 1)
	assert(if_type_else(list, min, max, (1, 2)) == 2)


def test_if_type():
	assert(if_type(list, min, [1, 2]) == 1)
	assert(if_type(tuple, min, [1, 2]) == [1, 2])


def test_if_list_else():
	assert(if_list_else(min, max, [1, 2]) == 1)
	assert(if_list_else(min, max, (1, 2)) == 2)


def test_if_list():
	assert(if_list(min, [1, 2]) == 1)
	assert(if_list(min, (1, 2)) == (1, 2))


def test_wrap_list():
	assert(wrap_list(None) == [None])


def test_ensure_list():
	assert(ensure_list(None) == [None])
	assert(ensure_list([None]) == [None])
	assert(ensure_list([[None]] == [[None]]))


def test_flatten_lists():
	assert(flatten_lists([]) == [])
	assert(flatten_lists([[], []]) == [])
	assert(flatten_lists([[1], [2]]) == [1, 2])
	assert(flatten_lists([[1], [2, 3]]) == [1, 2, 3])
	assert(flatten_lists([[1], [[2, 3]]]) == [1, [2, 3]])


def test_make_seq_of_flattened_lists():
	assert(make_seq_of_flattened_lists([1, 2, 3]) == [[1], [2], [3]])
	assert(make_seq_of_flattened_lists([1, [2, 3]]) == [[1], [2, 3]])
	assert(make_seq_of_flattened_lists([1, [2, [3]]]) == [[1], [2, 3]])


def test_only_types():
	assert(only_types([1, [], 1.5, None, 2.5, ()], [int, float, list]) == [1,[], 1.5, 2.5])


def test_only_ints():
	assert(only_ints([1, int, 2, "a", 3, print, 4, None, 5, [1, 2]]) == [1, 2, 3, 4, 5])


def test_none_if_empty():
	assert(none_if_empty([]) == None)
	assert(none_if_empty([1]) == [1])
	assert(none_if_empty([None]) == [None])

# Functions

def myreverse(x):
	"""A non-destructive reverse function takes a list as its input and it
	returns a reversed list at the top level as its output."""
	assert type(x) is list

	return x if len(x) == 0 else myreverse(x[1:]) + [x[0]]


def mirror(x):
	"""A function that takes a list (may be nested to any depth) and returns
	the mirror image of the list"""
	assert(type(x) is list)

	return [if_type(list, mirror, i) for i in myreverse(x)]


def flatten(x):
	"""A flatten function that takes a list (may be nested to any depth) and
	returns a list of one level only."""
	assert(type(x) is list)

	return flatten_lists(make_seq_of_flattened_lists(x))


def int_list(x):
	"""A filter function that takes a list as its input and returns a list of
	integers or None."""
	assert(type(x) is list)

	return none_if_empty(only_ints(x))


def invert_dict(x):
	"""A function that takes a dictionary as its input and returns a
	dictionary of inverted entries."""
	assert(type(x) is dict)

	return {v: k for k, v in x.items()}

# Test Functions

def test_myreverse():
	assert(myreverse([1, 2, 3]) == [3, 2, 1])
	assert(myreverse([[1, 2], [3, [4, 5]]]) == [[3, [4, 5]], [1, 2]])
	assert(myreverse([]) == [])


def test_mirror():
	assert(mirror([[1, 2], [3, [4, 5]]]) == [[[5, 4], 3], [2, 1]])
	assert(mirror([]) == [])


def test_flatten():
	assert(flatten([1]) == [1])
	assert(flatten([[1]]) == [1])
	assert(flatten([[[1]]]) == [1])
	assert(flatten([[1, 2], [3, [4, 5]]]) == [1, 2, 3, 4, 5])
	assert(flatten([]) == [])
	assert(flatten([1, 'a']) == [1, 'a'])


def test_int_list():
	assert(int_list([1, 'a', 2, 'b', [3, 4]]) == [1, 2])
	assert(int_list(['a', 'b', 'c']) == None)


def test_invert_dict():
	assert(invert_dict({}) == {})
	assert(invert_dict({1: 'a', 2: 'b', 3: 'c'}) == {'a': 1, 'b': 2, 'c': 3})

# Tests

def test_helper_functions():
	test_identity()
	test_if_type_else()
	test_if_type()
	test_if_list_else()
	test_if_list()
	test_wrap_list()
	test_ensure_list()
	test_flatten_lists()
	test_make_seq_of_flattened_lists()
	test_only_types()
	test_only_ints()
	test_none_if_empty()


def test_functions():
	test_myreverse()
	test_mirror()
	test_flatten()
	test_int_list()
	test_invert_dict()


def test_all():
	print("testing...")
	test_helper_functions()
	test_functions()
	print("test successful")


if __name__ == "__main__":
	test_all()

